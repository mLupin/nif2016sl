#include "rtf.h"

RTF::RTF() {
	this->buffer << "{\\rtf1\\ansi\\deff0 {\\fonttbl {\\f0 Calibri;}{\\f1 Verdana;}}{\\colortbl ; \\red255\\green0\\blue0; \\red0\\green255\\blue0;}";  //todo
}

void RTF::translateText(std::string text) {
	std::u16string conv;
	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> cv;
	conv = cv.from_bytes(text);
	for(char16_t c : conv) {
		if((int) c > 130)
			this->buffer << "\\u" << ((int) c) << "\\'3f"; 
		else
			this->buffer << (char) c;
	}
}

RTF& RTF::operator<<(std::string text) {
	this->translateText(text);
	return *this;
}

RTF& RTF::operator<<(int num) {
	if(num < 10 && num >= 0)
		this->buffer << "0"; 
		// brzydki kod
		// ale działa
	this->buffer << num;
	return *this;
}

RTF& RTF::operator<<(const char *chars) {
	this->translateText((std::string) chars);
	return *this;
}

RTF& RTF::operator<<(RTF &rtf) {
	return *this;
}

RTF& RTF::row() {
	this->buffer << "\\trowd\\trgraph250 ";
	return *this;
}

RTF& RTF::endrow() {
	this->buffer << "\\row ";
	return *this;
}

RTF& RTF::cells(int width, int n) {
	std::stringstream ret;
	for(int i = 1; i <= n; i++) {
		ret << "\\cellx" << width*i;
	}
	this->buffer << ret.str();
	return *this;
}

RTF& RTF::cell() {
	this->buffer << "\\pard\\intbl {";
	return *this;
}

RTF& RTF::endcell() {
	this->buffer << "}\\cell ";
	return *this;
}

RTF& RTF::par() {
	this->buffer << "{\\pard ";
	return *this;
}

RTF& RTF::endpar() {
	this->buffer << "\\par}";
	return *this;
}

RTF& RTF::bold() {
	this->buffer << "{\\b ";
	return *this;
}

RTF& RTF::italics() {
	this->buffer << "{\\i ";
	return *this;
}

RTF& RTF::alignleft() {
	this->buffer << "\\ql ";
	return *this;
}

RTF& RTF::aligncenter() {
	this->buffer << "\\qc ";
	return *this;
}

RTF& RTF::alignright() {
	this->buffer << "\\qr ";
	return *this;
}

RTF& RTF::font(int i) {
	this->buffer << "{\\f" << i << " ";
	return *this;
}

RTF& RTF::color(int i) {
	this->buffer << "{\\cf" << i << " ";
	return *this;
}

RTF& RTF::URL(std::string title, std::string url) {
	this->buffer << "{\\fielkd{\\*\\fldinst{HYPERLINK \"" << url << "\"}}{\\fldrslt{\\ul " << title << "}}}";
	return *this;
}

RTF& RTF::open() {
	this->buffer << "{";
	return *this;
}

RTF& RTF::close() {
	this->buffer << "}";
	return *this;
}

std::string RTF::print() {
	return this->buffer.str();
}

void RTF::tofile(std::string path) {
	std::ofstream file;
	file.open(path);
	file << this->buffer.str() + "\n";
	file.close();
}