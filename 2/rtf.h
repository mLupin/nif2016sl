#ifndef RTF_INC
#define RTF_INC
#include <iostream>
#include <string>
#include <sstream>
#include <codecvt>
#include <locale>
#include <cstdint>
#include <fstream>

class RTF {
public:
	RTF();
	RTF& operator<<(std::string);
	RTF& operator<<(int);
	RTF& operator<<(RTF&);
	RTF& operator<<(const char *);

	RTF& row();
	RTF& endrow();
	RTF& cells(int, int);
	RTF& cell();
	RTF& endcell();
	RTF& par();
	RTF& endpar();
	RTF& alignleft();
	RTF& aligncenter();
	RTF& alignright();
	RTF& font(int);
	RTF& color(int);
	RTF& URL(std::string, std::string);
	RTF& bold();
	RTF& italics();
	RTF& open();
	RTF& close();

	std::string print();
	void tofile(std::string);

private:
	std::stringstream buffer;
	void translateText(std::string);
};


#endif