#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <sstream>
#include <utility>
#include <iomanip>
#include "rtf.h"

struct Phone {
	std::string name;
	long number;
};
struct Person {
	std::string name;
	tm birthday;
	enum Sex { M, K } sex;
	float height;
	std::string email;
	std::vector<Phone> phones;
};

std::vector<std::string> explode(std::string const &, char);

int main() {
	setlocale(LC_ALL, "");
	std::ifstream infile("osoby.txt", std::ifstream::in);

	if(!infile.is_open()) {
		std::cerr << "Nie mozna otworzyc pliku wejsciowego" << std::endl;
		return 1;
	}

	std::string temp;
	std::vector<Person> db;
	while(getline(infile, temp)) {
		Person new_person;
		std::vector<std::string> data = explode(temp, ';');
		if(data.size() < 6) {
			std::cerr << "Niewystarczajace dane osoby" << std::endl;
			continue;
		}
		new_person.name = data[0];
		std::vector<std::string> birthday_data = explode(data[1], '.');
		// todo (kiedys): walidacja poprawnosci daty
		tm birthday;
		birthday.tm_mday = stoi(birthday_data[0]);
		birthday.tm_mon = stoi(birthday_data[1]);
		birthday.tm_year = stoi(birthday_data[2]) - 1900;
		new_person.birthday = birthday;
		if(data[2] == "M")
			new_person.sex = Person::M;
		else if(data[2] == "K")
			new_person.sex = Person::K;
		else {
			std::cerr << "Niepoprawna plec" << std::endl;
			continue;
		}
		new_person.height = stof(data[3]);
		new_person.email = data[4];
		std::vector<std::string> phones_data = explode(data[5],',');
		std::vector<Phone> phones;
		for(std::vector<std::string>::iterator phone_raw = phones_data.begin(); phone_raw != phones_data.end(); ++phone_raw) {  
			std::vector<std::string> phone_data = explode(*phone_raw, ' ');
			Phone phone;
			phone.name = phone_data[0];
			phone.number = stol(phone_data[1]);
			phones.push_back(phone);
		}
		new_person.phones = phones;
		db.push_back(new_person);
	}
	infile.close();

	RTF rtf;
	rtf << rtf.par() << rtf.endpar();

	int i = 0;
	for(std::vector<Person>::iterator person = db.begin(); person != db.end(); ++person) { 
		i++; 
		if(i%2 == 1) {
			if(i == db.size())
				rtf << rtf.row() << rtf.cells(4250, 1);
			else
				rtf << rtf.row() << rtf.cells(4250, 2);
		}

		rtf << rtf.cell();

		rtf << rtf.par() << rtf.aligncenter() << rtf.bold() << rtf.color(1) 
			<< person->name 
			<< rtf.close() << rtf.close() << rtf.endpar();

		rtf << rtf.par() << rtf.endpar();

		rtf << rtf.par() << rtf.alignleft()
			<< "Data urodzenia: " 
			<< rtf.italics() << person->birthday.tm_mday << "." << person->birthday.tm_mon << "." << 1900 + person->birthday.tm_year << " r." << rtf.close()
			<< rtf.endpar();

		rtf << rtf.par() << rtf.alignleft()
			<< "Płeć: " 
			<< rtf.italics() << (person->sex == Person::M ? "M" : "K") << rtf.close()
			<< rtf.endpar();

		rtf << rtf.par() << rtf.alignleft()
			<< "Wzrost: " 
			<< rtf.italics() << ((int) (person->height*100)) << " cm" << rtf.close()
			<< rtf.endpar();

		rtf << rtf.par() << rtf.alignleft()
			<< "Adres e-mail: " 
			<< rtf.italics() << rtf.URL(person->email, "mailto:" + person->email) << rtf.close()
			<< rtf.endpar();

		rtf << rtf.par() << rtf.alignleft() << rtf.color(2) << rtf.font(1)
			<< "Telefony: " 
			<< rtf.close() << rtf.close()
			<< rtf.endpar();

		for(std::vector<Phone>::iterator phone = person->phones.begin(); phone != person->phones.end(); ++phone) {
			rtf << rtf.par() << rtf.alignright() << rtf.font(1)
				<< phone->name << ": " 
				<< rtf.italics() << phone->number
				<< rtf.close() << rtf.close()
				<< rtf.endpar();
		}
		rtf << rtf.endcell();
		if(i%2 == 0 || i == db.size())
			rtf << rtf.endrow();
	}

	rtf << rtf.close();
	rtf.tofile("output.rtf");

	return 0;
}

std::vector<std::string> explode(std::string const & s, char delim) {
	std::vector<std::string> result;
	std::istringstream iss(s);

	for(std::string token; std::getline(iss, token, delim);) {
		result.push_back(std::move(token));
	}

	return result;
}