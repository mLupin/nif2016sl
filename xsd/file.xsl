<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <meta charset="UTF-8" />
	<link rel="stylesheet" type="text/css" href="file.css" />
      </head>
      <body>
	<h2>Osoby</h2>
	<table class="osoby">
	   <xsl:apply-templates />
	</table>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="person">
      <tr class="osoby"><td class="osoby">
      <table>
      <xsl:apply-templates select="name" />
      <tr><td>Sex:</td>
      <td><xsl:value-of select="@sex" /></td></tr>
      <xsl:apply-templates select="bdate" />
      <xsl:apply-templates select="height" />
      <xsl:apply-templates select="email" />
      <xsl:apply-templates select="phones" />
      </table>
      </td></tr>
  </xsl:template>
  <xsl:template match="name">
    <tr><td>Name:</td>
    <td><xsl:value-of select="." /></td></tr>
  </xsl:template>
  <xsl:template match="bdate">
    <tr><td>Date of birth:</td>
    <td><xsl:value-of select="." /></td></tr>
  </xsl:template>
  <xsl:template match="height">
    <tr><td>Height:</td>
    <td><xsl:value-of select="." /> cm</td></tr>
  </xsl:template>
  <xsl:template match="email">
    <tr><td>E-mail address:</td>
    <td class="email"><xsl:element name="a">
      <xsl:attribute name="href">
      <xsl:text>mailto:</xsl:text>
	<xsl:value-of select="." />
      </xsl:attribute>
      <xsl:value-of select="." />
    </xsl:element></td></tr>
  </xsl:template>
  <xsl:template match="phones">
    <tr><td>Phones:</td>
    <td></td></tr>
    <xsl:for-each select="*">
    <tr><td class="phonenum"><xsl:value-of select="local-name(.)" />:</td>
    <td><xsl:value-of select="." /></td></tr>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
