#!/usr/bin/env php
<?php
$styles = <<<EOF
	html, body {
		font-size: 14px;
		font-family: sans-serif;
	}

	a, a:link, a:active, a:visited {
		color: #000;
		text-decoration: none;
	}

	a:hover {
		text-decoration: underline;
	}

	.name {
		color: #ff1111;
		font-weight: bold;
		font-family: serif;
	}

	.phones {
		color: #11ff11;
	}

	.data {
		font-style: italic;
	}

	.aligncenter {
		text-align: center;
	}

	.alignright {
		text-align: right;
	}

	table {
		width: 80%;
		max-width: 600px;
		margin: 0 auto;
		margin-top: 20px;
	}

	td {
		padding: 10px;
	}
EOF;

$html = <<<EOF
	<!doctype html>
	<html>
	<head>
		<title>Zadanie 5 - Maciej Wilczyński 416200</title>
		<meta charset="utf-8" />
		{{stylesheet}}
	</head>
	<body>
		{{content}}
	</body>
	</html>
EOF;

$people_raw = file_get_contents("osoby.txt");
$people = array();
foreach(explode("\n", $people_raw) as $line) {
	$person_raw = explode(";", $line);
	$add = array();
	$add["name"] = $person_raw[0];
	$add["birthday"] = $person_raw[1];
	$add["sex"] = $person_raw[2];
	$add["height"] = $person_raw[3];
	$add["email"] = $person_raw[4];
	$add["phones"] = array();
	foreach(explode(",", $person_raw[5]) as $phone_raw) {
		$phone = explode(" ", $phone_raw);
		$add["phones"][$phone[0]] = $phone[1];
	}
	$people[] = $add;
}

$gen = "<table>\n";

foreach($people as $i => $person) {
	if($i % 2 == 0)
		$gen .= "<tr>\n";

	$gen .= "<td>";
	$gen .= 	"<p class=\"name aligncenter\">" . $person["name"] . "</p>\n";
	$gen .= 	"<p>\n";
	$gen .= 		"Data urodzenia: <span class=\"data\">" . $person["birthday"] . " r.</span><br/>\n";
	$gen .= 		"Płeć: <span class=\"data\">" . $person["sex"] . "</span><br/>\n";
	$gen .= 		"Wzrost: <span class=\"data\">" . $person["height"] . " m</span><br/>\n";
	$gen .= 		"Email: <span class=\"data\"><a href=\"mailto:" . $person["email"] . "\">" . $person["email"] . "</a></span><br/>\n";
	$gen .= 	"</p>\n";
	$gen .= 	"<p class=\"alignright\">\n";
	$gen .= 		"<span class=\"phones\">Telefony:</span><br/>\n";	
	foreach($person["phones"] as $name => $number)
		$gen .= 	"<span class=\"data\">" . $name . ":</span> " . $number . "<br/>\n";
	$gen .= 	"</p>\n";
	$gen .= "</td>\n";

	if($i+1 == count($people) && $i % 2 == 0)
		$gen .= "<td></td>\n";


	if($i % 2 == 1)
		$gen .= "</tr>\n\n";
}

$gen .= "</table>";

$output = str_replace("{{content}}", $gen, $html);
if(in_array("-c", $argv)) {
	file_put_contents("style.css", $styles);
	$output = str_replace("{{stylesheet}}", "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" media=\"screen\" />", $output);
} else 
	$output = str_replace("{{stylesheet}}", "<style>\n" . $styles . "\n</style>", $output);

file_put_contents("index.html", $output);



