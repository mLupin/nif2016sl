#!/usr/bin/env php
<?php

$people_raw = file_get_contents("osoby.txt");
$people = array();
foreach(explode("\n", $people_raw) as $line) {
	$person_raw = explode(";", $line);
	$add = array();
	$add["name"] = $person_raw[0];
	$add["birthday"] = $person_raw[1];
	$add["sex"] = $person_raw[2];
	$add["height"] = $person_raw[3];
	$add["email"] = $person_raw[4];
	$add["phones"] = array();
	foreach(explode(",", $person_raw[5]) as $phone_raw) {
		$phone = explode(" ", $phone_raw);
		$add["phones"][$phone[0]] = $phone[1];
	}
	$people[] = $add;
}

$xml = new SimpleXMLElement('<people/>');

foreach($people as $i => $person) {
	$person_xml = $xml->addChild("person");
	$person_xml->addAttribute("sex", $person["sex"]);
	$person_xml->addChild("name", $person["name"]);
	$person_xml->addChild("bdate", $person["birthday"]);
	$height = $person_xml->addChild("height", $person["height"]);
	$height->addAttribute("unit", "centimeters");
	$person_xml->addChild("email", $person["email"]);
	$xml_phones = $person_xml->addChild("phones");
	foreach($person["phones"] as $name => $number)
		$xml_phones->addChild($name, $number);
}

file_put_contents("output.xml", $xml->asXML());
