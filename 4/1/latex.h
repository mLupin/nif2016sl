#ifndef LATEX_INC
#define LATEX_INC
#include <iostream>
#include <string>
#include <sstream>
#include <codecvt>
#include <locale>
#include <cstdint>
#include <fstream>

class LaTeX {
public:
	LaTeX& init();
	LaTeX& operator<<(std::string);
	LaTeX& operator<<(int);
	LaTeX& operator<<(LaTeX&);
	LaTeX& operator<<(const char *);

	LaTeX& document();
	LaTeX& enddocument();
	LaTeX& center();
	LaTeX& endcenter();
	LaTeX& centering();
	LaTeX& endcentering();
	LaTeX& left();
	LaTeX& endleft();
	LaTeX& flushleft();
	LaTeX& endflushleft();
	LaTeX& flushright();
	LaTeX& endflushright();
	LaTeX& right();
	LaTeX& endright();
	LaTeX& table(std::string);
	LaTeX& endtable();
	LaTeX& hline();
	LaTeX& nl();
	LaTeX& endcell();
	LaTeX& bold();
	LaTeX& italics();
	LaTeX& underline();
	LaTeX& monospace();
	LaTeX& close();
	LaTeX& color(std::string);
	LaTeX& hyperlink(std::string, std::string);
	LaTeX& endl();

	std::string print();
	void tofile(std::string);

protected:
	std::stringstream buffer;
};


#endif
