#ifndef BEAMER_INC
#define BEAMER_INC
#include <iostream>
#include <string>
#include <sstream>
#include <codecvt>
#include <locale>
#include <cstdint>
#include <fstream>
#include "latex.h"

class Beamer : public LaTeX {
public:
	Beamer& init(std::string, std::string);
	Beamer& titlePage();
	Beamer& frame();
	Beamer& endframe();
};


#endif
