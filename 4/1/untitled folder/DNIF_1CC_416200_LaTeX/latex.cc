#include "latex.h"

LaTeX& LaTeX::init() {
	this->buffer << "\\documentclass{article}" << std::endl;
	this->buffer << "\\usepackage[utf8]{inputenc}" << std::endl;
	this->buffer << "\\usepackage{tabularx}" << std::endl;
	this->buffer << "\\usepackage{color}" << std::endl;
	this->buffer << "\\usepackage{hyperref}" << std::endl;
	this->buffer << "\\usepackage[T1]{fontenc}" << std::endl;
	return *this;
}

LaTeX& LaTeX::operator<<(std::string text) {
	this->buffer << text;
	return *this;
}

LaTeX& LaTeX::operator<<(int num) {
	if(num < 10 && num >= 0)
		this->buffer << "0"; 
		// brzydki kod
		// ale działa
	this->buffer << num;
	return *this;
}

LaTeX& LaTeX::operator<<(const char *chars) {
	this->buffer << ((std::string) chars);
	return *this;
}

LaTeX& LaTeX::operator<<(LaTeX &ltx) {
	return *this;
}

LaTeX& LaTeX::document() {
	this->buffer << "\\begin{document}" << std::endl;
	return *this;
}

LaTeX& LaTeX::enddocument() {
	this->buffer << "\\end{document}" << std::endl;
	return *this;
}

LaTeX& LaTeX::center() {
	this->buffer << "\\begin{center}";
	return *this;
}

LaTeX& LaTeX::endcenter() {
	this->buffer << "\\end{center}";
	return *this;
}

LaTeX& LaTeX::centering() {
	this->buffer << "{\\centering ";
	return *this;
}

LaTeX& LaTeX::endcentering() {
	this->buffer << " \\par}";
	return *this;
}

LaTeX& LaTeX::left() {
	this->buffer << "{\\raggedright ";
	return *this;
}

LaTeX& LaTeX::endleft() {
	this->buffer << " \\par}";
	return *this;
}

LaTeX& LaTeX::flushleft() {
	this->buffer << "\\begin{flushleft} ";
	return *this;
}

LaTeX& LaTeX::endflushleft() {
	this->buffer << " \\end{flushleft}";
	return *this;
}

LaTeX& LaTeX::flushright() {
	this->buffer << "\\begin{flushright} ";
	return *this;
}

LaTeX& LaTeX::endflushright() {
	this->buffer << " \\end{flushright}";
	return *this;
}

LaTeX& LaTeX::right() {
	this->buffer << "{\\raggedleft ";
	return *this;
}

LaTeX& LaTeX::endright() {
	this->buffer << " \\par}";
	return *this;
}

LaTeX& LaTeX::table(std::string cols) {
	this->buffer << "\\begin{tabularx}{\\textwidth}{" << cols << "}" << std::endl;
	return *this;
}

LaTeX& LaTeX::endtable() {
	this->buffer << "\\end{tabularx}" << std::endl;
	return *this;
}
	
LaTeX& LaTeX::nl() {
	this->buffer << " \\\\\n";
	return *this;
}

LaTeX& LaTeX::hline() {
	this->buffer << "\\hline" << std::endl;
	return *this;
}

LaTeX& LaTeX::endcell() {
	this->buffer << std::endl << "&" << std::endl;
	return *this;
}

LaTeX& LaTeX::bold() {
	this->buffer << "\\textbf{";
	return *this;
}

LaTeX& LaTeX::italics() {
	this->buffer << "\\textit{";
	return *this;
}

LaTeX& LaTeX::underline() {
	this->buffer << "\\underline{";
	return *this;
}

LaTeX& LaTeX::monospace() {
	this->buffer << " \\ttfamily ";
	return *this;
}

LaTeX& LaTeX::color(std::string name) {
	this->buffer << "{\\color{" + name + "} ";
	return *this;
}

LaTeX& LaTeX::hyperlink(std::string url, std::string text) {
	this->buffer << "\\href{" + url + "}{" + text + "}";
	return *this;
}

LaTeX& LaTeX::endl() {
	this->buffer << " \\par" << std::endl;
	return *this;
}

LaTeX& LaTeX::close() {
	this->buffer << "}";
	return *this;
}

std::string LaTeX::print() {
	return this->buffer.str();
}

void LaTeX::tofile(std::string path) {
	std::ofstream file;
	file.open(path);
	file << this->buffer.str() + "\n";
	file.close();
}