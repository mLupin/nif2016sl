#include "beamer.h"

Beamer& Beamer::init(std::string title, std::string subtitle) {
	this->buffer << "\\documentclass{beamer}" << std::endl;
	this->buffer << "\\usepackage[utf8]{inputenc}" << std::endl;
	this->buffer << "\\usepackage{tabularx}" << std::endl;
	this->buffer << "\\usepackage{color}" << std::endl;
	this->buffer << "\\usepackage[T1]{fontenc}" << std::endl;
	this->buffer << "\\title{" << title << "}" << std::endl;
	this->buffer << "\\subtitle{" << subtitle << "}" << std::endl;
	this->buffer << "\\date[]" << std::endl << std::endl;
	return *this;
}

Beamer& Beamer::titlePage() {
	this->buffer << "\\frame{\\titlepage}" << std::endl;
	return *this;
}

Beamer& Beamer::frame() {
	this->buffer << "\\begin{frame}" << std::endl;
	return *this;
}

Beamer& Beamer::endframe() {
	this->buffer << "\\end{frame}" << std::endl;
	return *this;
}
