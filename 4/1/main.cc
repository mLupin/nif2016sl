#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <sstream>
#include <utility>
#include <iomanip>
#include "latex.h"
#include "beamer.h"

struct Phone {
	std::string name;
	long number;
};
struct Person {
	std::string name;
	tm birthday;
	enum Sex { M, K } sex;
	float height;
	std::string email;
	std::vector<Phone> phones;
};

std::vector<std::string> explode(std::string const &, char);

int main() {
	setlocale(LC_ALL, "");
	std::ifstream infile("osoby.txt", std::ifstream::in);

	if(!infile.is_open()) {
		std::cerr << "Nie mozna otworzyc pliku wejsciowego" << std::endl;
		return 1;
	}

	std::string temp;
	std::vector<Person> db;
	while(getline(infile, temp)) {
		Person new_person;
		std::vector<std::string> data = explode(temp, ';');
		if(data.size() < 6) {
			std::cerr << "Niewystarczajace dane osoby" << std::endl;
			continue;
		}
		new_person.name = data[0];
		std::vector<std::string> birthday_data = explode(data[1], '.');
		// todo (kiedys): walidacja poprawnosci daty
		tm birthday;
		birthday.tm_mday = stoi(birthday_data[0]);
		birthday.tm_mon = stoi(birthday_data[1]);
		birthday.tm_year = stoi(birthday_data[2]) - 1900;
		new_person.birthday = birthday;
		if(data[2] == "M")
			new_person.sex = Person::M;
		else if(data[2] == "K")
			new_person.sex = Person::K;
		else {
			std::cerr << "Niepoprawna plec" << std::endl;
			continue;
		}
		new_person.height = stof(data[3]);
		new_person.email = data[4];
		std::vector<std::string> phones_data = explode(data[5],',');
		std::vector<Phone> phones;
		for(std::vector<std::string>::iterator phone_raw = phones_data.begin(); phone_raw != phones_data.end(); ++phone_raw) {  
			std::vector<std::string> phone_data = explode(*phone_raw, ' ');
			Phone phone;
			phone.name = phone_data[0];
			phone.number = stol(phone_data[1]);
			phones.push_back(phone);
		}
		new_person.phones = phones;
		db.push_back(new_person);
	}
	infile.close();

	LaTeX ltx;
	ltx << ltx.init();
	ltx << ltx.document();
	ltx << ltx.center();
	ltx << ltx.table("| X | X |");
	ltx << ltx.hline();
	Beamer bmr;
	bmr << bmr.init("Maciej Wilczyński", "416200");
	bmr << bmr.document();
	bmr << bmr.titlePage();

	int i = 0;
	for(std::vector<Person>::iterator person = db.begin(); person != db.end(); ++person) { 
		i++; 

		bmr << bmr.frame();

		ltx << ltx.centering() << ltx.bold() << ltx.color("red") << ltx.monospace() 
			<< person->name
			<< ltx.close() << ltx.close() << ltx.endcentering();

		bmr << bmr.bold()
			<< person->name
			<< bmr.close();

		ltx << ltx.flushleft();
		bmr << bmr.flushleft();

		ltx << "Data urodzenia: " 
			<< ltx.italics() 
			<< person->birthday.tm_mday << "." << person->birthday.tm_mon << "." << 1900 + person->birthday.tm_year << " r." 
			<< ltx.close() << ltx.endl();

		bmr << "Data urodzenia: " 
			<< bmr.italics() 
			<< person->birthday.tm_mday << "." << person->birthday.tm_mon << "." << 1900 + person->birthday.tm_year << " r." 
			<< bmr.close() << bmr.endl();

		ltx << "Płeć: "
			<< ltx.italics()
			<< (person->sex == Person::M ? "M" : "K")
			<< ltx.close() << ltx.endl();

		bmr << "Płeć: "
			<< bmr.italics()
			<< (person->sex == Person::M ? "M" : "K")
			<< bmr.close() << bmr.endl();

		ltx << "Wzrost: "
			<< ltx.italics()
			<< ((int) (person->height*100)) << " cm" 
			<< ltx.close() << ltx.endl();

		bmr << "Wzrost: "
			<< bmr.italics()
			<< ((int) (person->height*100)) << " cm" 
			<< bmr.close() << bmr.endl();

		ltx << "Adres e-mail: "
			<< ltx.italics()
			<< ltx.hyperlink("mailto:" + person->email, person->email)
			<< ltx.close() << ltx.endl();

		bmr << "Adres e-mail: "
			<< bmr.italics()
			<< bmr.hyperlink("mailto:" + person->email, person->email)
			<< bmr.close() << bmr.endl();

		ltx << ltx.endflushleft();
		bmr << bmr.endflushleft();

		ltx << ltx.flushright();

		ltx << ltx.color("green")
			<< "Telefony:"
			<< ltx.close() << ltx.endl();

		bmr << bmr.color("green")
			<< "Telefony:"
			<< bmr.close() << bmr.endl();

		for(std::vector<Phone>::iterator phone = person->phones.begin(); phone != person->phones.end(); ++phone) {
			ltx << phone->name << ": " 
				<< ltx.italics() 
				<< phone->number
				<< ltx.close() << ltx.endl();

			bmr << phone->name << ": " 
				<< bmr.italics() 
				<< phone->number
				<< bmr.close() << bmr.endl();
		}

		ltx << ltx.endflushright();

		bmr << bmr.endframe();

		if(i%2 == 0)
			ltx << ltx.nl() << ltx.hline();
		else if(i == db.size())
			ltx << ltx.endcell() << ltx.nl() << ltx.hline();
		else 
			ltx << ltx.endcell();

	}

	ltx << ltx.endtable();
	ltx << ltx.endcenter();
	ltx << ltx.enddocument();
	bmr << bmr.enddocument();
	ltx.tofile("zadanie_1.tex");
	bmr.tofile("zadanie_2.tex");

	return 0;
}

std::vector<std::string> explode(std::string const & s, char delim) {
	std::vector<std::string> result;
	std::istringstream iss(s);

	for(std::string token; std::getline(iss, token, delim);) {
		result.push_back(std::move(token));
	}

	return result;
}