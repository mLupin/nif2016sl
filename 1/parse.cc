#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <sstream>
#include <utility>
#include <iomanip>

struct Phone {
	std::string name;
	long number;
};
struct Person {
	std::string name;
	tm birthday;
	enum Sex { M, K } sex;
	float height;
	std::string email;
	std::vector<Phone> phones;
};

std::vector<std::string> explode(std::string const &, char);

int main() {
	std::ifstream infile("osoby.txt", std::ifstream::in);

	if(!infile.is_open()) {
		std::cerr << "Nie mozna otworzyc pliku wejsciowego" << std::endl;
		return 1;
	}

	std::string temp;
	std::vector<Person> db;
	while(getline(infile, temp)) {
		Person new_person;
		std::vector<std::string> data = explode(temp, ';');
		if(data.size() < 6) {
			std::cerr << "Niewystarczajace dane osoby" << std::endl;
			continue;
		}
		new_person.name = data[0];
		std::vector<std::string> birthday_data = explode(data[1], '.');
		// todo (kiedys): walidacja poprawnosci daty
		tm birthday;
		birthday.tm_mday = stoi(birthday_data[0]);
		birthday.tm_mon = stoi(birthday_data[1]);
		birthday.tm_year = stoi(birthday_data[2]) - 1900;
		new_person.birthday = birthday;
		if(data[2] == "M")
			new_person.sex = Person::M;
		else if(data[2] == "K")
			new_person.sex = Person::K;
		else {
			std::cerr << "Niepoprawna plec" << std::endl;
			continue;
		}
		new_person.height = stof(data[3]);
		new_person.email = data[4];
		std::vector<std::string> phones_data = explode(data[5],',');
		std::vector<Phone> phones;
		for(std::vector<std::string>::iterator phone_raw = phones_data.begin(); phone_raw != phones_data.end(); ++phone_raw) {  
			std::vector<std::string> phone_data = explode(*phone_raw, ' ');
			Phone phone;
			phone.name = phone_data[0];
			phone.number = stol(phone_data[1]);
			phones.push_back(phone);
		}
		new_person.phones = phones;
		db.push_back(new_person);
	}
	infile.close();

	int i = 0;
	for(std::vector<Person>::iterator person = db.begin(); person != db.end(); ++person) { 
		i++; 
		std::cout << "Osoba nr " << i << ":" << std::endl;
		std::cout << "\t" << "Imie i nazwisko: " << person->name << std::endl;
		std::cout << "\t" <<  "Data urodzenia: " << std::setw(2) << std::setfill('0') << person->birthday.tm_mday << "." << 
					std::setw(2) << std::setfill('0') << person->birthday.tm_mon << "." << 
					1900 + person->birthday.tm_year << std::endl;
		std::cout << "\t" <<  "Plec: " << (person->sex == Person::M ? "M" : "K") << std::endl;
		std::cout << "\t" <<  "Wzrost: " << std::setprecision(2) << person->height << "m" << std::endl;
		std::cout << "\t" <<  "Adres e-mail: " << person->email << std::endl;
		std::cout << "\t" <<  "Telefony:" << std::endl;
		for(std::vector<Phone>::iterator phone = person->phones.begin(); phone != person->phones.end(); ++phone) {
			std::cout << "\t\t" <<  phone->name << ": " << phone->number << std::endl;
		}
		std::cout << std::endl;
	}

	return 0;
}

std::vector<std::string> explode(std::string const & s, char delim) {
	std::vector<std::string> result;
	std::istringstream iss(s);

	for(std::string token; std::getline(iss, token, delim);) {
		result.push_back(std::move(token));
	}

	return result;
}