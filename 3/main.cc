#include <iostream>
#include <fstream>
#include <vector>
#include "graph.h"
#include "utils.h"

int main() {
    std::fstream output("output.ps", std::ios::out),
        input("osoby.txt", std::ios::in);

    int circle_r = 30, line_width = 200, people_count = 0;
    std::vector<char> sex;
    std::vector<float> height;
    std::vector<Vec2i> pos;

    std::string line;
    while(getline(input, line, ';')) 
    {
        getline(input, line, ';'); 
        getline(input, line, ';'); 
        sex.push_back(line[0]);

        getline(input, line, ';'); 
        height.push_back(atof(line.c_str()));

        getline(input, line, ';'); 
        getline(input, line);

        people_count++;
    }

    Graph graph;
    graph.text(50, 800, "Zadanie 3");
    graph.setText("newpath\n");

    float x,y;
    for(int i = 0; i < people_count; ++i) {
        x = (sin(2.f * 3.14f / people_count * i) * 200) + 280;
        y = (cos(2.f * 3.14f / people_count * i) * 200) + 400;
        pos.push_back(Vec2i(x,y));

        graph.text(x-8, y-8, itos(i+1));
        graph.setText("newpath\n");
        graph.circle(x, y, circle_r);
    }

    for(int i = 0; i < people_count; ++i) {
        for(int j = 0; j < people_count; ++j) {
            if(i != j && sex[i] == sex[j] && height[i] < height[j]) {
                int angle = atan2(pos[j].y - pos[i].y, pos[j].x - pos[i].x) * 180.f / 3.14f;
                if(angle < 0)
                    angle += 360;
                graph.arow(pos[i].x, pos[i].y, circle_r, distance(pos[j].x, pos[j].y, pos[i].x, pos[i].y) - circle_r * 2, angle);
            }
        }
    }

    output << graph.getText();
    output.close();
    input.close();
    return 0;
}
