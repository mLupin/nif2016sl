#ifndef UTILS_INC
#define UTILS_INC

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <string>

class Vec2i
{
public:
    int x, y;
    Vec2i(int _x, int _y)
        : x(_x), y(_y) {}
};

float distance(float,float, float,float);
std::string itos(int);


#endif
