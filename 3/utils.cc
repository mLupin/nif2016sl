#include "utils.h"

float distance(float dX0, float dY0, float dX1, float dY1) {
    return sqrt((dX1 - dX0)*(dX1 - dX0) + (dY1 - dY0)*(dY1 - dY0));
}

std::string itos(int i) {
    std::string tmp;
    sprintf((char*) tmp.c_str(), "%d", i);
    std::string str = tmp.c_str();
    return str;
}
