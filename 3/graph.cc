#include "graph.h"
#include "utils.h"

Graph::Graph() {
    content = "%!PS\n";
}

void Graph::circle(int x, int y, int size) {
    content += itos(x) + " " + itos(y) + " " + itos(size) + " 0 360 arc\n";
    content += "2 setlinewidth\n";
    content += "stroke\n";
}

void Graph::arow(int x, int y, int circle_r, int length, int angle) {
   content += "gsave\n" + itos(x) + " " + itos(y) + " translate\n" + itos(angle) + " rotate\n\
                " + itos(circle_r) + " 0 moveto\n\
                " + itos(length) + " 0 rlineto\n\
                -35 15 rlineto\n\
                0 -30 rlineto\n\
                35 15 rlineto\n\
                1 setlinewidth\n\
                stroke\n\
                grestore\n";
}

void Graph::text(int x, int y, std::string text) {
    content += "/Courier 25 selectfont\n\
                " + itos(x) + " " + itos(y) + " moveto\n\
                (" + text + ") show\n";
}

std::string Graph::getText() {
    content += "showpage";
    return content;
}

void Graph::setText(std::string text) {
    content += text;
}
