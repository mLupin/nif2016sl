#ifndef GRAPH_INC
#define GRAPH_INC
#include <string>

class Graph {
public:
    Graph();
    void circle(int,int, int);
    void arow(int,int, int, int,int);
    void text(int,int, std::string);
    void setText(std::string);
    std::string getText();

protected:
    std::string content;
};


#endif
